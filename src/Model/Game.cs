using System;
using System.Collections.Generic;

namespace Battleship
{
    public class Game
    {
        IPlayer john;
        IPlayer jane;
        IInput input;

        public Game(IPlayer john, IPlayer jane, IInput input)
        {
            this.john = john;
            this.jane = jane;
            this.input = input;
        }

        public Game() : this(new Player(), new Player(), new Input()){ }

        public void Start()
        {
            john.Initialize();
            jane.Initialize();

            var outcome = default(Outcome);
            var players = new Queue<IPlayer>();

            players.Enqueue(john);
            players.Enqueue(jane);

            while (!(outcome == Outcome.GameOver))
            {
                outcome = NextTurn(players);
                input.Put(Messages.For[outcome]);
            }
        }

        Outcome NextTurn(Queue<IPlayer> players)
        {
            var currPlayer = players.Dequeue();
            var nextPlayer = players.Peek();

            players.Enqueue(currPlayer);

            return nextPlayer.RespondTo(currPlayer.Shoot());
        }
    }
}
