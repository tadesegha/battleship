using System;
using System.Collections.Generic;

namespace Battleship
{
    public class Board : IBoard
    {
        IInput input;
        IShipFactory shipFactory;
        IShip ship;
        IList<Coordinate> hits = new List<Coordinate>();

        public Board(IInput input, IShipFactory shipFactory)
        {
            this.input = input;
            this.shipFactory = shipFactory;
        }

        public Board() : this(new Input(), new ShipFactory()){ }

        public void PlaceShip(string name)
        {
            do
            {
                var location = input.Get(Messages.BattleshipLocation, name);
                ship = shipFactory.CreateAt(location);
            }
            while (!ship.LocationValid);
        }

        public Outcome IsHit(Coordinate coordinate)
        {
            if (ship.IsAtLocation(coordinate) && !hits.Contains(coordinate))
            {
                hits.Add(coordinate);
                return Outcomes.For[hits.Count];
            }

            return Outcome.Miss;
        }
    }
}
