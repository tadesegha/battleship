namespace Battleship
{
    public interface IShip
    {
        bool LocationValid { get; }
        bool IsAtLocation(Coordinate coordinate);
        Coordinate[] Location { get; }
    }
}
