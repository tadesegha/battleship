using NUnit.Framework;
using System.Collections.Generic;
using Battleship;
using Rhino.Mocks;

namespace BattleshipTests
{
    [TestFixture]
    public class ValidatorTests
    {
        [TestCase("A1", "A2", "A3", true)]
        [TestCase("A1", "B1", "C1", true)]
        [TestCase("A1", "B2", "C1", false)]
        public void OneFulfilledValidatonMeansValidationIsSuccessful(string point0, string point1, string point2, bool expected)
        {
            var horizontalLocation = new Coordinate[]
            {
                new Coordinate(){ x = 'A', y = '1' },
                new Coordinate(){ x = 'B', y = '1' },
                new Coordinate(){ x = 'C', y = '1' }
            };
            var verticalLocation = new Coordinate[]
            {
                new Coordinate(){ x = 'A', y = '1' },
                new Coordinate(){ x = 'A', y = '2' },
                new Coordinate(){ x = 'A', y = '3' }
            };
            var invalidLocation = new Coordinate[]
            {
                new Coordinate(){ x = 'A', y = '1' },
                new Coordinate(){ x = 'B', y = '2' },
                new Coordinate(){ x = 'C', y = '1' }
            };

            var horizontalValidation = MockRepository.GenerateMock<IValidation>();
            var verticalValidation = MockRepository.GenerateMock<IValidation>();

            horizontalValidation.Expect(x => x.Matches(verticalLocation)).Return(false);
            horizontalValidation.Expect(x => x.Matches(horizontalLocation)).Return(true);
            horizontalValidation.Expect(x => x.Matches(invalidLocation)).Return(false);

            verticalValidation.Expect(x => x.Matches(verticalLocation)).Return(true);
            verticalValidation.Expect(x => x.Matches(horizontalLocation)).Return(false);
            verticalValidation.Expect(x => x.Matches(invalidLocation)).Return(false);

            var validations = new List<IValidation>{ horizontalValidation, verticalValidation };
            var sut = new Validator(validations);

            var location = new Coordinate[]
            {
                new Coordinate(){ x = point0[0], y = point0[1] },
                new Coordinate(){ x = point1[0], y = point1[1] },
                new Coordinate(){ x = point2[0], y = point2[1] }
            };
            Assert.AreEqual(sut.IsValid(location), expected);
        }
    }
}
