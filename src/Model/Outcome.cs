namespace Battleship
{
    public enum Outcome
    {
        Miss, Hit, GameOver
    }
}
