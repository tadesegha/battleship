using System;
using System.Linq;
using System.Collections.Generic;

namespace Battleship
{
    public class Player : IPlayer
    {
        IInput input;
        IBoard board;
        string name;

        public Player(IInput input, IBoard board)
        {
            this.input = input;
            this.board = board;
        }

        public Player() : this(new Input(), new Board()){ }

        public void Initialize()
        {
            name = input.Get(Messages.Name);
            board.PlaceShip(name);
        }

        public Coordinate Shoot()
        {
            var response = input.Get(Messages.Shoot, name);
            return new Coordinate(){ x = response[0], y = response[1] };
        }

        public Outcome RespondTo(Coordinate move)
        {
            return board.IsHit(move);
        }
    }
}
