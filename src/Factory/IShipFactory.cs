namespace Battleship
{
    public interface IShipFactory
    {
        IShip CreateAt(string location);
    }
}
