using System;

namespace Battleship
{
    public class Coordinate
    {
        public char x { get; set; }
        public char y { get; set; }

        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var coordinate = (Coordinate) obj;

            return x.Equals(coordinate.x) &&
                y.Equals(coordinate.y);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
