using System;
using System.Collections.Generic;

namespace Battleship
{
    public class Validator : IValidator
    {
        IList<IValidation> validations;

        public Validator(IList<IValidation> validations)
        {
            this.validations = validations;
        }

        public Validator() : this(new List<IValidation>())
        {
            var horizontalValidation = new HorizontalValidation();
            var verticalValidation = new VerticalValidation();

            validations.Add(horizontalValidation);
            validations.Add(verticalValidation);
        }

        public bool IsValid(Coordinate[] location)
        {
            var isValid = false;

            foreach (var validation in validations)
                isValid = isValid || validation.Matches(location);

            return isValid;
        }
    }
}
