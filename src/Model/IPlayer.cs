namespace Battleship
{
    public interface IPlayer
    {
        void Initialize();
        Coordinate Shoot();
        Outcome RespondTo(Coordinate shot);
    }
}
