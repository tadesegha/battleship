namespace Battleship
{
    public interface IValidator
    {
        bool IsValid(Coordinate[] location);
    }
}
