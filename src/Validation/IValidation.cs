namespace Battleship
{
    public interface IValidation
    {
        bool Matches(Coordinate[] location);
    }
}
