using NUnit.Framework;
using Battleship;
using Rhino.Mocks;

namespace BattleshipTests
{
    [TestFixture]
    public class BoardTests
    {
        IInput input;
        IShipFactory shipFactory;
        IBoard sut;

        [Test]
        public void WhenAShipIsPlacedOnABoard_TheShipShouldBePlacedAtAValidLocation()
        {
            var expectedNumTries = 15;
            var numRetries = 0;
            var name = "name";
            var location = "location";

            var invalidShip = MockRepository.GenerateMock<IShip>();
            var validShip = MockRepository.GenerateMock<IShip>();

            input.Expect(x => x.Get(Messages.BattleshipLocation, name)).Return(location);

            shipFactory.Expect(x => x.CreateAt(location)).WhenCalled(x => numRetries++)
                .Return(invalidShip).Repeat.Times(expectedNumTries - 1);
            shipFactory.Expect(x => x.CreateAt(location)).WhenCalled(x => numRetries++)
                .Return(validShip);

            invalidShip.Expect(x => x.LocationValid).Return(false);
            validShip.Expect(x => x.LocationValid).Return(true);

            sut.PlaceShip(name);

            Assert.AreEqual(expectedNumTries, numRetries);
        }

        [Test]
        public void AMissOutcomeShouldBeReturnedWhenABattleshipIsMissed()
        {
            var isHit = false;
            var coordinate = new Coordinate(){ x = 'A', y = '1' };

            SetUpForOutcome(new Coordinate[]{coordinate}, isHit);
            Assert.AreEqual(Outcome.Miss, sut.IsHit(coordinate));
        }

        [Test]
        public void AHitOutcomeShouldBeReturnedWhenABattleshipIsHit()
        {
            var isHit = true;
            var coordinate = new Coordinate(){ x = 'A', y = '1' };

            SetUpForOutcome(new Coordinate[]{coordinate}, isHit);
            Assert.AreEqual(Outcome.Hit, sut.IsHit(coordinate));
        }

        [Test]
        public void AHitOutcomeShouldBeReturnedWhenABattleshipHasAlreadyBeenHitAtACoordinate()
        {
            var isHit = true;
            var coordinate = new Coordinate(){ x = 'A', y = '1' };

            SetUpForOutcome(new Coordinate[]{coordinate}, isHit);
            sut.IsHit(coordinate);
            Assert.AreEqual(Outcome.Miss, sut.IsHit(coordinate));
        }

        [Test]
        public void AGameOverOutcomeShouldBeReturnedWhenABattleshipIsSunk()
        {
            var isHit = true;

            var coordinate1 = new Coordinate(){ x = 'A', y = '1' };
            var coordinate2 = new Coordinate(){ x = 'A', y = '2' };
            var coordinate3 = new Coordinate(){ x = 'A', y = '3' };

            var coordinates = new Coordinate[] { coordinate1, coordinate2, coordinate3 };

            SetUpForOutcome(coordinates, isHit);
            sut.IsHit(coordinate1);
            sut.IsHit(coordinate2);

            Assert.AreEqual(Outcome.GameOver, sut.IsHit(coordinate3));
        }

        [SetUp]
        public void SetUp()
        {
            input = MockRepository.GenerateMock<IInput>();
            shipFactory = MockRepository.GenerateMock<IShipFactory>();

            sut = new Board(input, shipFactory);
        }

        public void SetUpForOutcome(Coordinate[] coordinates, bool isAtLocation)
        {
            var name = "name";
            var location = "location";

            var ship = MockRepository.GenerateMock<IShip>();

            input.Expect(x => x.Get(Messages.BattleshipLocation, name)).Return(location);

            shipFactory.Expect(x => x.CreateAt(location)).Return(ship);

            ship.Expect(x => x.LocationValid).Return(true);

            foreach (var coordinate in coordinates)
                ship.Expect(x => x.IsAtLocation(coordinate)).Return(isAtLocation);

            sut.PlaceShip(name);
        }
    }
}
