using NUnit.Framework;
using Battleship;
using Rhino.Mocks;

namespace BattleshipTests
{
    [TestFixture]
    public class ShipFactoryTests
    {
        [Test]
        public void WhenCreatingAShip_TheLocationForTheShipShouldBeCorrectlyDetermined()
        {
            var location = "A1, A2, A3";
            var coordinates = new Coordinate[]
            {
                new Coordinate(){ x = 'A', y = '1' },
                new Coordinate(){ x = 'A', y = '2' },
                new Coordinate(){ x = 'A', y = '3' }
            };

            var sut = new ShipFactory();
            var ship = sut.CreateAt(location);

            Assert.AreEqual(coordinates, ship.Location);
        }
    }
}
