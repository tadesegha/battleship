using System;

namespace Battleship
{
    public class HorizontalValidation : IValidation
    {
        public bool Matches(Coordinate[] location)
        {
            var startPoint = location[0];

            return location.Length == 3 &&
                location[1].x == (startPoint.x + 1) &&
                location[1].y == startPoint.y &&
                location[2].x == (startPoint.x + 2) &&
                location[2].y == startPoint.y;
        }
    }
}
