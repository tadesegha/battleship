using NUnit.Framework;
using Battleship;
using Rhino.Mocks;

namespace BattleshipTests
{
    [TestFixture]
    public class GameTests
    {
        Game sut;
        IPlayer john;
        IPlayer jane;
        IInput input;

        [SetUp]
        public void SetUp()
        {
            john = MockRepository.GenerateMock<IPlayer>();
            jane = MockRepository.GenerateMock<IPlayer>();
            input = MockRepository.GenerateMock<IInput>();

            sut = new Game(john, jane, input);
        }

        [Test]
        public void WhenAGameIsStarted_AllPlayersShouldBeInitialized()
        {
            TypicalSetup();

            john.AssertWasCalled(x => x.Initialize());
            jane.AssertWasCalled(x => x.Initialize());
        }

        [Test]
        public void PlayersShouldMakeAMoveAlternativelyTillGameIsOver()
        {
            var numTurnsJohn = 0;
            var numTurnsJane = 0;
            var numTurns = 15;

            var johnMove = new Coordinate(){ x = 'A', y = '1' };
            var janeMove = new Coordinate(){ x = 'B', y = '1' };

            john.Stub(x => x.Shoot()).WhenCalled(x => { numTurnsJohn++; }).Return(johnMove);
            john.Stub(x => x.RespondTo(janeMove)).Return(Outcome.Miss).Repeat.Times(numTurns);
            john.Stub(x => x.RespondTo(janeMove)).Return(Outcome.GameOver);

            jane.Stub(x => x.Shoot()).WhenCalled(x => { numTurnsJane++; }).Return(janeMove);
            jane.Stub(x => x.RespondTo(johnMove)).Return(Outcome.Miss);

            sut.Start();

            Assert.AreEqual(++numTurns, numTurnsJohn);
            Assert.AreEqual(numTurns, numTurnsJane);
        }

        [Test]
        public void WhenABattleshipIsSunk_ThePlayersShouldBeInformed()
        {
            TypicalSetup();
            input.AssertWasCalled(x => x.Put(Messages.BattleshipSunk));
        }

        [Test]
        public void WithEachTurn_ThePlayersShouldBeInformedOfOutcomes()
        {
            var move = new Coordinate(){ x = 'A', y = '1' };

            john.Expect(x => x.Shoot()).Return(move);
            john.Expect(x => x.RespondTo(move)).Return(Outcome.Hit);

            jane.Expect(x => x.RespondTo(move)).Return(Outcome.Miss).Repeat.Once();
            jane.Expect(x => x.RespondTo(move)).Return(Outcome.GameOver);
            jane.Expect(x => x.Shoot()).Return(move);

            sut.Start();
            input.AssertWasCalled(x => x.Put(Messages.For[Outcome.Hit]));
            input.AssertWasCalled(x => x.Put(Messages.For[Outcome.Miss]));
            input.AssertWasCalled(x => x.Put(Messages.For[Outcome.GameOver]));
        }

        void TypicalSetup()
        {
            var move = new Coordinate(){ x = 'C', y = '1' };

            john.Expect(x => x.Shoot()).Return(move);
            jane.Expect(x => x.RespondTo(move)).Return(Outcome.GameOver);

            sut.Start();
        }
    }
}
