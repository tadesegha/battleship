using NUnit.Framework;
using Battleship;
using Rhino.Mocks;

namespace BattleshipTests
{
    [TestFixture]
    public class ShipTests
    {
        Coordinate[] location;
        IValidator validator;
        IShip sut;

        [Test]
        public void ValidationShouldBeDelegated()
        {
            var result = sut.LocationValid;
            validator.AssertWasCalled(x => x.IsValid(location));
        }

        [TestCase('A', '1', true)]
        public void ShipsShouldBeAbleToReportOnItsLocation(char x, char y, bool expected)
        {
            var coordinate = new Coordinate(){ x = x, y = y };

            Assert.AreEqual(expected, sut.IsAtLocation(coordinate));
        }

        [SetUp]
        public void SetUp()
        {
            location = new Coordinate[]
            {
                new Coordinate(){ x = 'A', y = '1' },
                new Coordinate(){ x = 'B', y = '1' },
                new Coordinate(){ x = 'C', y = '1' }
            };

            validator = MockRepository.GenerateMock<IValidator>();

            sut = new Ship(validator){ Location = location };
        }
    }
}
