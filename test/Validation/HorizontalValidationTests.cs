using NUnit.Framework;
using Battleship;

namespace BattleshipTests
{
    [TestFixture]
    public class HorizontalValidationTests
    {
        [Test]
        public void AHorizontalLocationShouldBeValid()
        {
            var location = new Coordinate[]
            {
                new Coordinate(){ x = 'E', y = '1' },
                new Coordinate(){ x = 'F', y = '1' },
                new Coordinate(){ x = 'G', y = '1' }
            };
            var sut = new HorizontalValidation();

            Assert.True(sut.Matches(location));
        }

        [Test]
        public void ALocationShouldHaveThreePoints()
        {
            var location = new Coordinate[]
            {
                new Coordinate(){ x = 'E', y = '1' },
                new Coordinate(){ x = 'F', y = '1' }
            };
            var sut = new HorizontalValidation();

            Assert.False(sut.Matches(location));
        }
    }
}
