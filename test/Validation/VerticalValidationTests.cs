using NUnit.Framework;
using Battleship;

namespace BattleshipTests
{
    [TestFixture]
    public class VerticalValidationTests
    {
        [Test]
        public void AVerticalLocationShouldBeValid()
        {
            var location = new Coordinate[]
            {
                new Coordinate(){ x = 'E', y = '1' },
                new Coordinate(){ x = 'E', y = '2' },
                new Coordinate(){ x = 'E', y = '3' }
            };
            var sut = new VerticalValidation();

            Assert.True(sut.Matches(location));
        }

        [Test]
        public void ALocationShouldHaveThreePoints()
        {
            var location = new Coordinate[]
            {
                new Coordinate(){ x = 'E', y = '1' },
                new Coordinate(){ x = 'E', y = '2' } 
            };
            var sut = new VerticalValidation();

            Assert.False(sut.Matches(location));
        }
    }
}
