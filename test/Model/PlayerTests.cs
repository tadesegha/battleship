using System;
using NUnit.Framework;
using Rhino.Mocks;
using Battleship;

namespace BattleshipTests
{
    [TestFixture]
    public class PlayerTests
    {
        IPlayer sut;
        IInput input;
        IBoard board;
        string name;

        [Test]
        public void WhenAPlayerIsInitialized_ThePlayerShouldChooseTheirName()
        {
            TypicalSetup();
            input.AssertWasCalled(x => x.Get(Messages.Name));
        }

        [Test]
        public void WhenAPlayerIsInitialized_ThePlayerShouldPlaceTheirShipOnTheBoard()
        {
            TypicalSetup();
            board.AssertWasCalled(x => x.PlaceShip(name));
        }

        [Test]
        public void WhenAPlayerRespondsToAMiss_TheOutcomeOfTheMoveShouldBeAMiss()
        {
            TypicalSetup();

            var coordinate = new Coordinate(){ x = 'A', y = '1' };

            board.Expect(x => x.IsHit(coordinate)).Return(Outcome.Miss);

            Assert.AreEqual(Outcome.Miss, sut.RespondTo(coordinate));
        }

        [Test]
        public void WhenAPlayerRespondsToAHit_TheOutcomeOfTheMoveShouldBeAHit()
        {
            TypicalSetup();

            var coordinate = new Coordinate(){ x = 'B', y = '2' };
            
            board.Expect(x => x.IsHit(coordinate)).Return(Outcome.Hit);

            Assert.AreEqual(Outcome.Hit, sut.RespondTo(coordinate));
        }

        [Test]
        public void WhenAPreviousHitIsRespondedToAgain_TheOutcomeOfTheMoveShouldBeAMiss()
        {
            TypicalSetup();

            var coordinate = new Coordinate(){ x = 'B', y = '2' };

            board.Expect(x => x.IsHit(coordinate)).Return(Outcome.Hit).Repeat.Once();
            board.Expect(x => x.IsHit(coordinate)).Return(Outcome.Miss);

            sut.RespondTo(coordinate);

            Assert.AreEqual(Outcome.Miss, sut.RespondTo(coordinate));
        }

        [Test]
        public void WhenABattleshipIsSunk_TheOutcomeOfTheMoveShouldBeGameOver()
        {
            TypicalSetup();

            var coordinate = new Coordinate(){ x = 'B', y = '1' };

            board.Expect(x => x.IsHit(coordinate)).Return(Outcome.GameOver);

            var outcome = sut.RespondTo(coordinate);

            Assert.AreEqual(Outcome.GameOver, outcome);
        }

        [Test]
        public void WhenAPlayerPlays_TheyShouldBePromptedForAMove()
        {
            TypicalSetup();
            sut.Shoot();

            input.AssertWasCalled(x => x.Get(Messages.Shoot, name));
        }

        [SetUp]
        public void SetUp()
        {
            input = MockRepository.GenerateMock<IInput>();
            board = MockRepository.GenerateMock<IBoard>();

            sut = new Player(input, board);
        }

        void TypicalSetup()
        {
            name = "player name";
            var move = "A1";
            var coordinates = new Coordinate[]
            {
                new Coordinate(){ x = 'B', y = '1' },
                new Coordinate(){ x = 'B', y = '2' },
                new Coordinate(){ x = 'B', y = '3' }
            };

            input.Expect(x => x.Get(Messages.Name)).Return(name);
            input.Expect(x => x.Get(Messages.Shoot, name)).Return(move);

            sut.Initialize();
        }
    }
}
