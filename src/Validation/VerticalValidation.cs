using System;

namespace Battleship
{
    public class VerticalValidation : IValidation
    {
        public bool Matches(Coordinate[] location)
        {
            var startPoint = location[0];

            return location.Length == 3 &&
                location[1].x == startPoint.x &&
                location[1].y == (startPoint.y + 1) &&
                location[2].x == startPoint.x &&
                location[2].y == (startPoint.y + 2);
        }
    }
}
