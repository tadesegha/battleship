using System.Collections.Generic;

namespace Battleship
{
    public static class Outcomes
    {
        public static IDictionary<int, Outcome> For = new Dictionary<int, Outcome>
        {
            { 1, Outcome.Hit },
            { 2, Outcome.Hit },
            { 3, Outcome.GameOver }
        };
    }
}
