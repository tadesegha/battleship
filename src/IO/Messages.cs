using System.Collections.Generic;

namespace Battleship
{
    public static class Messages
    {
        public const string BattleshipLocation = "Please enter the location for your battleship (e.g. G1, G2, G3)";
        public const string Shoot = "Please enter a move";
        public const string Name = "Player name";
        public const string BattleshipSunk = "you sunk my battleship";

        public static Dictionary<Outcome, string>For = new Dictionary<Outcome, string>
        {
            { Outcome.Hit, "Hit"},
            { Outcome.Miss, "Miss"},
            { Outcome.GameOver, "you sunk my battleship"}
        };
    }
}
