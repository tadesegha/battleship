using System;

namespace Battleship
{
    public class Input : IInput
    {
        public string Get(string message)
        {
            Console.Write("{0}: ", message);
            return Console.ReadLine().Replace(" ", String.Empty).ToUpper();
        }

        public string Get(string message, string name)
        {
            return Get(String.Format("({0}) {1}", name, message));
        }

        public void Put(string message)
        {
            Console.WriteLine(message);
        }
    }
}
