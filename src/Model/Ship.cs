using System;
using System.Linq;
using System.Collections.Generic;

namespace Battleship
{
    public class Ship : IShip
    {
        IValidator validator;

        public Ship(IValidator validator)
        {
            this.validator = validator;
        }

        public Ship() : this(new Validator()){ }

        public bool LocationValid
        {
            get { return validator.IsValid(Location); }
        }

        public bool IsAtLocation(Coordinate coordinate)
        {
            return Location.Contains(coordinate);
        }

        public Coordinate[] Location { get; set; }
    }
}
