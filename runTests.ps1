﻿function clean {
  if (test-path ./bin){
    rmdir ./bin -recurse -force
  }

  mkdir bin | out-null
}

function compileSrc {
  & csc.exe /nologo /t:exe /out:bin\Battleship.exe /recurse:src\*.cs
}

function compileTests {
  & csc.exe /nologo /t:library /out:bin\BattleshipTests.dll /reference:lib\nunit.framework\bin\net-4.5\nunit.framework.dll,lib\rhino.mocks.dll,bin\Battleship.exe /recurse:test\*.cs
}

function exitOnFail {
  if ($LASTEXITCODE -ne 0){
    exit
  }
}

function executeTests {
  copy ./lib/nunit.framework/bin/net-4.5/* ./bin
  copy ./lib/rhino.mocks.dll ./bin

  & ./lib/nunit.console/nunit3-console.exe ./bin/BattleshipTests.dll
}

clean
compileSrc
exitOnFail
compileTests
exitOnFail
executeTests
