namespace Battleship
{
    public interface IInput
    {
        string Get(string message);
        string Get(string message, string name);
        void Put(string message);
    }
}
