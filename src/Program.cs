using System;

namespace Battleship
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("\n-----------");
            Console.Out.WriteLine("Assumptions");
            Console.Out.WriteLine("-----------");
            Console.Out.WriteLine("1. Only validation around battleship location is done\n  -Battleship can be placed horizontally or vertically\n  -Battleship location must consist of three contiguous points\n");

            new Game().Start();
        }
    }
}
