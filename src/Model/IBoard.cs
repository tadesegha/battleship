namespace Battleship
{
    public interface IBoard
    {
        void PlaceShip(string name);
        Outcome IsHit(Coordinate coordinate);
    }
}
