using System;

namespace Battleship
{
    public class ShipFactory : IShipFactory
    {
        public IShip CreateAt(string location)
        {
            var locationArray = location.Replace(" ", string.Empty).Split(',');

            var coordinates = new Coordinate[locationArray.Length];

            for (var index = 0; index < locationArray.Length; index++)
            {
                var currCoordinate = locationArray[index];
                coordinates[index] = new Coordinate(){ x = currCoordinate[0], y = currCoordinate[1] };
            }

            var ship = new Ship(){ Location = coordinates };

            return ship;
        }
    }
}
